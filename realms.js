var http = require('http');
var rlms = { eu:[],
                us:[]
    };
var realms = function realms(region,cb) {
    if(rlms[region].length === 0){
        var url = 'http://'+region+'.battle.net/api/wow/realm/status?json=?';
        http.get(url,function(resp){
            var str = '';
            resp.on('data',function(chnk){
                str += chnk;
            });
            resp.on('end', function(){
                
                JSON.parse(str).realms.forEach(function(e){
                    rlms[region].push(e.name);
                });
                cb(rlms[region]);    
            });
            
        });    
    }else{
        cb(rlms[region]);    
    }
};
module.exports = realms;