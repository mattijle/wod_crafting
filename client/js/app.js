var parseWowUction = function(file,cb){
    var reader = new FileReader()
    reader.onload = function(){
        var res = this.result.split('\n');
        var fieldNames = _.slice(res,0,1)[0].split(',');
        var ahRows = _.slice(res,1,res.length);
        var byId = {};
        var byName = {};
        _.map(ahRows,function(e,i){
            var row = e.split(',');
            var zip = _.zipObject(fieldNames,row);
            var id = zip['Item ID'];
            var name = zip['Item Name'];
            byId[id] = zip;
            byName[name] = zip;
        });
        cb({byName:byName,byId:byId});
    };
    reader.readAsText(file);
};
var app = angular.module('WoDSpreadsheet',['ngStorage']);
app.directive('file', function() {
    return {
        require:"ngModel",
        restrict: 'A',
        link: function($scope, el, attrs, ngModel){
            el.bind('change', function(event){
                var files = event.target.files;
                var file = files[0];
                parseWowUction(file, function(d){
                    $scope.wowuction = d;
                    //$scope.$apply();
                });
                
            });
        }
    };
});
app.directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                      iElement.trigger('input');
                    }, 0);
                }
            });
    };
});
app.service('WoWUctionService',function($http){
    this.getData = function(url){
        if(typeof url !== 'undefined'){
            console.log(url.url);
            return $http.post('/wowuction',{
                data: JSON.stringify(url.url),
                contentType : 'application/json'
            }).then(function(data){
                return data;
            });
        }
    };
});
app.controller('RealmController',function($scope,$localStorage){
    var realmList = {
        EU: ["Aegwynn","Aerie Peak","Agamaggan","Aggra (Português)","Aggramar","Ahn'Qiraj","Al'Akir","Alexstrasza","Alleria","Alonsus","Aman'Thul","Ambossar","Anachronos","Anetheron","Antonidas","Anub'arak","Arak-arahm","Arathi","Arathor","Archimonde","Area 52","Argent Dawn","Arthas","Arygos","Ashenvale","Aszune","Auchindoun","Azjol-Nerub","Azshara","Azuregos","Azuremyst","Baelgun","Balnazzar","Blackhand","Blackmoore","Blackrock","Blackscar","Blade's Edge","Bladefist","Bloodfeather","Bloodhoof","Bloodscalp","Blutkessel","Booty Bay","Borean Tundra","Boulderfist","Bronze Dragonflight","Bronzebeard","Burning Blade","Burning Steppes","C'Thun","Chamber of Aspects","Chants éternels","Cho'gall","Chromaggus","Colinas Pardas","Confrérie du Thorium","Conseil des Ombres","Crushridge","Culte de la Rive noire","Daggerspine","Dalaran","Dalvengyr","Darkmoon Faire","Darksorrow","Darkspear","Das Konsortium","Das Syndikat","Deathguard","Deathweaver","Deathwing","Deepholm","Defias Brotherhood","Dentarg","Der Mithrilorden","Der Rat von Dalaran","Der abyssische Rat","Destromath","Dethecus","Die Aldor","Die Arguswacht","Die Nachtwache","Die Silberne Hand","Die Todeskrallen","Die ewige Wacht","Doomhammer","Draenor","Dragonblight","Dragonmaw","Drak'thul","Drek'Thar","Dun Modr","Dun Morogh","Dunemaul","Durotan","Earthen Ring","Echsenkessel","Eitrigg","Eldre'Thalas","Elune","Emerald Dream","Emeriss","Eonar","Eredar","Eversong","Executus","Exodar","Festung der Stürme","Fordragon","Forscherliga","Frostmane","Frostmourne","Frostwhisper","Frostwolf","Galakrond","Garona","Garrosh","Genjuros","Ghostlands","Gilneas","Goldrinn","Gordunni","Gorgonnash","Greymane","Grim Batol","Grom","Gul'dan","Hakkar","Haomarush","Hellfire","Hellscream","Howling Fjord","Hyjal","Illidan","Jaedenar","Kael'thas","Karazhan","Kargath","Kazzak","Kel'Thuzad","Khadgar","Khaz Modan","Khaz'goroth","Kil'jaeden","Kilrogg","Kirin Tor","Kor'gall","Krag'jin","Krasus","Kul Tiras","Kult der Verdammten","La Croisade écarlate","Laughing Skull","Les Clairvoyants","Les Sentinelles","Lich King","Lightbringer","Lightning's Blade","Lordaeron","Los Errantes","Lothar","Madmortem","Magtheridon","Mal'Ganis","Malfurion","Malorne","Malygos","Mannoroth","Marécage de Zangar","Mazrigos","Medivh","Minahonda","Moonglade","Mug'thol","Nagrand","Nathrezim","Naxxramas","Nazjatar","Nefarian","Nemesis","Neptulon","Ner'zhul","Nera'thor","Nethersturm","Nordrassil","Norgannon","Nozdormu","Onyxia","Outland","Perenolde","Pozzo dell'Eternità","Proudmoore","Quel'Thalas","Ragnaros","Rajaxx","Rashgarroth","Ravencrest","Ravenholdt","Razuvious","Rexxar","Runetotem","Sanguino","Sargeras","Saurfang","Scarshield Legion","Sen'jin","Shadowsong","Shattered Halls","Shattered Hand","Shattrath","Shen'dralar","Silvermoon","Sinstralis","Skullcrusher","Soulflayer","Spinebreaker","Sporeggar","Steamwheedle Cartel","Stormrage","Stormreaver","Stormscale","Sunstrider","Sylvanas","Taerar","Talnivarr","Tarren Mill","Teldrassil","Temple noir","Terenas","Terokkar","Terrordar","The Maelstrom","The Sha'tar","The Venture Co","Theradras","Thermaplugg","Thrall","Throk'Feroth","Thunderhorn","Tichondrius","Tirion","Todeswache","Trollbane","Turalyon","Twilight's Hammer","Twisting Nether","Tyrande","Uldaman","Ulduar","Uldum","Un'Goro","Varimathras","Vashj","Vek'lor","Vek'nilash","Vol'jin","Wildhammer","Wrathbringer","Xavius","Ysera","Ysondre","Zenedar","Zirkel des Cenarius","Zul'jin","Zuluhed"],
        US: ["Aegwynn","Aerie Peak","Agamaggan","Aggramar","Akama","Alexstrasza","Alleria","Altar of Storms","Alterac Mountains","Aman'Thul","Andorhal","Anetheron","Antonidas","Anub'arak","Anvilmar","Arathor","Archimonde","Area 52","Argent Dawn","Arthas","Arygos","Auchindoun","Azgalor","Azjol-Nerub","Azralon","Azshara","Azuremyst","Baelgun","Balnazzar","Barthilas","Black Dragonflight","Blackhand","Blackrock","Blackwater Raiders","Blackwing Lair","Blade's Edge","Bladefist","Bleeding Hollow","Blood Furnace","Bloodhoof","Bloodscalp","Bonechewer","Borean Tundra","Boulderfist","Bronzebeard","Burning Blade","Burning Legion","Caelestrasz","Cairne","Cenarion Circle","Cenarius","Cho'gall","Chromaggus","Coilfang","Crushridge","Daggerspine","Dalaran","Dalvengyr","Dark Iron","Darkspear","Darrowmere","Dath'Remar","Dawnbringer","Deathwing","Demon Soul","Dentarg","Destromath","Dethecus","Detheroc","Doomhammer","Draenor","Dragonblight","Dragonmaw","Drak'Tharon","Drak'thul","Draka","Drakkari","Dreadmaul","Drenden","Dunemaul","Durotan","Duskwood","Earthen Ring","Echo Isles","Eitrigg","Eldre'Thalas","Elune","Emerald Dream","Eonar","Eredar","Executus","Exodar","Farstriders","Feathermoon","Fenris","Firetree","Fizzcrank","Frostmane","Frostmourne","Frostwolf","Galakrond","Gallywix","Garithos","Garona","Garrosh","Ghostlands","Gilneas","Gnomeregan","Goldrinn","Gorefiend","Gorgonnash","Greymane","Grizzly Hills","Gul'dan","Gundrak","Gurubashi","Hakkar","Haomarush","Hellscream","Hydraxis","Hyjal","Icecrown","Illidan","Jaedenar","Jubei'Thos","Kael'thas","Kalecgos","Kargath","Kel'Thuzad","Khadgar","Khaz Modan","Khaz'goroth","Kil'jaeden","Kilrogg","Kirin Tor","Korgath","Korialstrasz","Kul Tiras","Laughing Skull","Lethon","Lightbringer","Lightning's Blade","Lightninghoof","Llane","Lothar","Madoran","Maelstrom","Magtheridon","Maiev","Mal'Ganis","Malfurion","Malorne","Malygos","Mannoroth","Medivh","Misha","Mok'Nathal","Moon Guard","Moonrunner","Mug'thol","Muradin","Nagrand","Nathrezim","Nazgrel","Nazjatar","Nemesis","Ner'zhul","Nesingwary","Nordrassil","Norgannon","Onyxia","Perenolde","Proudmoore","Quel'Thalas","Quel'dorei","Ragnaros","Ravencrest","Ravenholdt","Rexxar","Rivendare","Runetotem","Sargeras","Saurfang","Scarlet Crusade","Scilla","Sen'jin","Sentinels","Shadow Council","Shadowmoon","Shadowsong","Shandris","Shattered Halls","Shattered Hand","Shu'halo","Silver Hand","Silvermoon","Sisters of Elune","Skullcrusher","Skywall","Smolderthorn","Spinebreaker","Spirestone","Staghelm","Steamwheedle Cartel","Stonemaul","Stormrage","Stormreaver","Stormscale","Suramar","Tanaris","Terenas","Terokkar","Thaurissan","The Forgotten Coast","The Scryers","The Underbog","The Venture Co","Thorium Brotherhood","Thrall","Thunderhorn","Thunderlord","Tichondrius","Tol Barad","Tortheldrin","Trollbane","Turalyon","Twisting Nether","Uldaman","Uldum","Undermine","Ursin","Uther","Vashj","Vek'nilash","Velen","Warsong","Whisperwind","Wildhammer","Windrunner","Winterhoof","Wyrmrest Accord","Ysera","Ysondre","Zangarmarsh","Zul'jin","Zuluhed"]
    };
    $scope.$storage = $localStorage;
    $scope.$storage.region = $scope.$storage.region||'EU';
    $scope.realms = realmList[$scope.$storage.region];
});
app.controller('shuffleController',function($scope, $localStorage,WoWUctionService) {
    $scope.$storage = $localStorage;
    WoWUctionService.getData($scope.$storage.data).then(function(data){
        console.log(data.data);
    });
    /*$scope.$storage.ahLookUp = $scope.$storage.ahLookUp||[];
    var bloodShuffleIds = ['109118','109119','109124','109125','109126','109127',
            '109128','109129','109693','110609','111557'];
    $scope.$storage.bloodShuffle = $scope.$storage.bloodShuffle ||[];
    var init = function(){
        if(_.pluck($scope.$storage.ahLookUp,'id').length === 0){
            _.map(bloodShuffleIds,function(i){
                $scope.addItem(i);
            });
        }
        if(typeof $scope.$storage.data !== 'undefined'){
            $scope.postUrl();
        }    
        setInterval(function(){
            if(typeof $scope.wowuction !== 'undefined'){
                if(typeof $scope.wowuction.byId !== 'undefined'){
                    var ahkeys = _.pluck($scope.$storage.ahLookUp,'id');

                    $scope.$storage.ahLookUp = _.map(ahkeys,function(i){
                        var item = {id: i};
                        item.name = $scope.wowuction.byId[item.id]['Item Name'];
                        item.marketPrice = $scope.wowuction.byId[item.id]['AH MarketPrice'];
                        item.minPrice = $scope.wowuction.byId[item.id]['AH MinimumPrice'];
                        return item;
                    });
                    $scope.$storage.bloodShuffle = _.filter($scope.$storage.ahLookUp,function(n){
                        return bloodShuffleIds.indexOf(n.id) !== -1;
                    });
                    $scope.$storage.bloodShuffle = _.map($scope.$storage.bloodShuffle,function(e){
                        e.primal = e.marketPrice *5;
                        e.savage = e.primal * 50;
                        e.profit =  $scope.wowuction.byName['Savage Blood']['AH MarketPrice'] - e.savage;
                        return e;
                    });
                    $scope.$apply();
                }        
            }
        },10);
        setInterval(function() {
            $scope.postUrl();
        },60*60*1000);   
    };

    $scope.addItem = function(it){
        it = it || $scope.newItem
        $.getJSON('https://eu.battle.net/api/wow/item/'+it+'?jsonp=?',function(data){
            var item = {};
            item.name = data.name;
            item.id = it;
            if(typeof $scope.wowuction !== 'undefined' && typeof $scope.wowuction.byId !== undefined){
                item.marketPrice = $scope.wowuction.byId[item.id]['AH MarketPrice'];
                item.minPrice = $scope.wowuction.byId[item.id]['AH MinimumPrice'];
            }
            $scope.$storage.ahLookUp.push(item);
            if(bloodShuffleIds.indexOf(it) !== -1){
                $scope.$storage.bloodShuffle.push(item);
            }
            $scope.$apply();    
        });
    };
    $scope.postUrl = function(){
            $.ajax({
                url:'/wowuction',
                type:'POST',
                data: JSON.stringify($scope.$storage.data),
                contentType : 'application/json',
                success: function(res){
                    console.log('Loading wowuction data.');
                    $scope.parseWowUction(res);
                }
            });
    };
    $scope.parseWowUction = function(data){
        data = data.split('\n');
        var fieldNames = _.slice(data,0,1)[0].split(',');
        var ahRows = _.slice(data,1,data.length);
        $scope.wowuction = {};
        $scope.wowuction.byId = $scope.wowuction.byId || {};
        $scope.wowuction.byName = $scope.wowuction.byName || {};
        _.map(ahRows,function(e,i){
            var row = e.split(',');
            var zip = _.zipObject(fieldNames,row);
            var id = zip['Item ID'];
            var name = zip['Item Name'];
            $scope.wowuction.byId[id] = zip;
            $scope.wowuction.byName[name] = zip;
        });
/*
        var names = Object.keys($scope.wowuction.byName);
        var ids = Object.keys($scope.wowuction.byId);
        var str = '['
        $scope.$storage.wow = _.map(names,function(e){
            str += "{name:'"+e.replace("'","\'").replace('"','\"')+"',id:"+$scope.wowuction.byName[e]['Item ID']+'},';
            return {name:e,id:$scope.wowuction.byName[e]['Item ID']};
        });
        str += ']';
        console.log(str);
        $scope.$apply();
    };
    init();
    */
});
